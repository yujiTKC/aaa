//
//  data_ex.m
//  ts_test1_0
//
//  Created by 滝内 雄二 on 2013/07/11.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import "data_ex.h"

@implementation data_ex

@synthesize description = _description;
@synthesize title = _title;
@synthesize name = _name;
@synthesize address = _address;
@synthesize brief = _brief;
@synthesize url = _url;
@synthesize period = _period;
@synthesize mapurl = _mapurl;

- (void)manageDescription{
//    NSLog(@"START manageDescription");
    NSArray *Strs = [_description componentsSeparatedByString:@", "];
/* 0723
    for (NSString *i in Strs) {
        NSLog(@"%@",i);
    }
0723 */
    _name = [Strs[0] substringFromIndex:9];
    _brief = [Strs[3] substringFromIndex:7];
    _address = [Strs[4] substringFromIndex:9];
    _url = [Strs[5] substringFromIndex:8];
    _mapurl = [Strs[6] substringFromIndex:9];
    
    NSString *sdate = [Strs[1] substringFromIndex:11];
    NSString *edate = [Strs[2] substringFromIndex:10];
    
    /*
    NSLog(@"name is %@",_name);
    NSLog(@"brie is %@",_brief);
    NSLog(@"addr is %@",_address);
    NSLog(@"url  is %@",_url);
    NSLog(@"murl is %@",_mapurl);
    */
     //元データに[, ]がないことをチェックする必要有
    
    NSLocale *jaLocale;
    jaLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"];
    NSDateFormatter *fmt1 = [[NSDateFormatter alloc]init];
    [fmt1 setDateFormat:@"yyyy年MM月dd日"];
    NSDateFormatter *fmt2 = [[NSDateFormatter alloc]init];
    [fmt2 setDateFormat:@"yyyy/MM/dd"];
    NSDate *dateS = [fmt2 dateFromString:sdate];
    NSDate *dateE = [fmt2 dateFromString:edate];
    NSString *strS = [fmt1 stringFromDate:dateS];
    NSString *strE = [fmt1 stringFromDate:dateE];
    _period = [[NSMutableString alloc] initWithCapacity:30];
    [_period appendString:strS];
    [_period appendString:@" ~ "];
    [_period appendString:strE];
    

}

@end