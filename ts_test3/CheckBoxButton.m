//
//  CheckBoxButton.m
//  ts_test1_0
//
//  Created by 滝内 雄二 on 2013/07/23.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import "CheckBoxButton.h"

@implementation CheckBoxButton

@synthesize checkBoxSelected = _checkBoxSelected;

- (void)awakeFromNib
{
    [super awakeFromNib];
    UIImage* nocheck = [UIImage imageNamed:@"nocheck.png"];
    UIImage* checked = [UIImage imageNamed:@"check.png"];
    UIImage* disable = [UIImage imageNamed:@"disable_check.png"];
    [self setBackgroundImage:nocheck forState:UIControlStateNormal];
    [self setBackgroundImage:checked forState:UIControlStateSelected];
    [self setBackgroundImage:checked forState:UIControlStateHighlighted];
    [self setBackgroundImage:disable forState:UIControlStateDisabled];
    [self addTarget:self action:@selector(checkboxPush:) forControlEvents:UIControlEventTouchUpInside];
    [self setState:self];
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self) {
        // Initialization code
//        NSLog(@"initial CheckBox");
        UIImage* nocheck = [UIImage imageNamed:@"nocheck.png"];
        UIImage* checked = [UIImage imageNamed:@"check.png"];
        UIImage* disable = [UIImage imageNamed:@"disable_check.png"];
        
        [self setBackgroundImage:nocheck forState:UIControlStateNormal];
        [self setBackgroundImage:checked forState:UIControlStateSelected];
        [self setBackgroundImage:checked forState:UIControlStateHighlighted];
        [self setBackgroundImage:disable forState:UIControlStateDisabled];
        [self addTarget:self action:@selector(checkboxPush:) forControlEvents:UIControlEventTouchUpInside];
//        NSLog(@"initial");
        [self setState:self val:YES];
        
    }
    return self;
}

- (void)checkboxPush:(CheckBoxButton*) button {
    button.checkBoxSelected = !button.checkBoxSelected;
    NSLog(@"checkboxpush");
    [button setState:button];
}

- (void)setState:(CheckBoxButton*) button
{
    if (button.checkBoxSelected) {
        [button setSelected:YES];
    } else {
        [button setSelected:NO];
    }
}

- (void)setState:(CheckBoxButton*) button val:(BOOL)b{
//    NSLog(@"val =  %@ ",b);
    if (button.checkBoxSelected || b ) {
        [button setSelected:YES];
    }else{
        [button setSelected:NO];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
