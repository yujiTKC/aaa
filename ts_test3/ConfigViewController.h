//
//  ConfigViewController.h
//  ts_test1_0
//
//  Created by 滝内 雄二 on 2013/07/28.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import <UIKit/UIKit.h>
#define REGION_ARRAY [NSArray arrayWithObjects: @"北海道",@"東北",@"北陸",@"関東",@"中部",@"近畿",@"中国",@"四国",@"九州",nil]
@interface ConfigViewController : UITableViewController{

    bool boolRegion[9];
}
- (IBAction)test:(UIBarButtonItem *)sender;
@end
