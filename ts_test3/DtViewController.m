//
//  DtViewController.m
//  ts_test4
//
//  Created by 滝内 雄二 on 2013/06/28.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import "DtViewController.h"
#import "AdViewController.h"
#import "Exhibition.h"

@interface DtViewController ()

@end

@implementation DtViewController

@synthesize thisKey = _thisKey;
@synthesize cell1 = _cell1;
@synthesize cell2 = _cell2;
@synthesize cell3 = _cell3;
@synthesize cell4 = _cell4;
@synthesize cell5 = _cell5;
@synthesize selectedCellNum = _selectedCellNum;
@synthesize briefText = _briefText;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization

    }
    
    
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSArray *vcArray = self.navigationController.viewControllers;
    Ex = ((AdViewController *)[vcArray objectAtIndex:0]).exhibition;
    fields = ALL_FIELDS_ARRAY;
    
    self.tableView.backgroundColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor blueColor];
    
    NSLog(@"DtViewLoad sNum %u",_selectedCellNum);
    


    _cell1.textLabel.text = [Ex.dicData[_selectedCellNum] objectForKey:@"name"];
    _cell2.textLabel.text = [Ex.dicData[_selectedCellNum] objectForKey:@"period"];
    _cell3.textLabel.text = [Ex.dicData[_selectedCellNum] objectForKey:@"address"];
//    _cell4.textLabel.text = [Ex.dicData[_selectedCellNum] objectForKey:@"brief"];
    _cell5.textLabel.text = [Ex.dicData[_selectedCellNum] objectForKey:@"url"];


    _cell1.textLabel.numberOfLines = 0;
    _cell2.textLabel.numberOfLines = 0;
    _cell3.textLabel.numberOfLines = 0;
    _cell4.textLabel.numberOfLines = 0;
    _cell5.textLabel.numberOfLines = 0;

    
    _briefText.text = [Ex.dicData[_selectedCellNum] objectForKey:@"brief"];
    
    /*
    NSArray *keys = [NSArray arrayWithObjects:@"name",@"brief",@"url",@"period", nil];*/

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@",[Ex.dicData[_selectedCellNum] objectForKey:@"name" ]];
    
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

/*
 - (void)viewDidLoad で作成しているセル編集系ボタンの設置作業を削除（コメントアウト。以下同。）します。
 - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView をメソッド丸ごと削除します。 *
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section をメソッド丸ごと削除します。*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath をメソッド丸ごと削除します。 *
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath をメソッド丸ごと削除します。*
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath をメソッド丸ごと削除します。
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender を、segueのIdentifierに合わせて調整します。基本的には、DetailViewControllerに渡すデータの処理をしています。
 */




/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *thisKey1 = [Ex.keynum objectForKey:[NSString stringWithFormat:@"%d",section] ];
    NSLog(@"calling: tableView:titleForHeaderInSection %@" ,thisKey1);
//    NSLog(@"check1 %@ , %@",[Ex.namedic objectForKey:thisKey1],thisKey1);
//    NSLog(@"dic %@",[Ex.namedic objectForKey:thisKey1]);
//    return _thisKey;
    

    return [Ex.namedic objectForKey:thisKey1]; 
    return @"aa";
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}



@end
