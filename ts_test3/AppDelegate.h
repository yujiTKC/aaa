//
//  AppDelegate.h
//  ts_test3
//
//  Created by 滝内 雄二 on 2013/06/27.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
