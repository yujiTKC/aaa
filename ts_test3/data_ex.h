//
//  data_ex.h
//  ts_test1_0
//
//  Created by 滝内 雄二 on 2013/07/11.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface data_ex : NSObject

@property (strong,nonatomic) NSString *description;
@property (strong,nonatomic) NSString *title;
@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) NSString *address;
@property (strong,nonatomic) NSString *brief;
@property (strong,nonatomic) NSString *url;
@property (strong,nonatomic) NSMutableString *period;
@property (strong,nonatomic) NSString *mapurl;

- (void)manageDescription;

@end
