//
//  Exhibition.h
//  ts_test3
//
//  Created by 滝内 雄二 on 2013/06/27.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "data_ex.h"

#define ALL_FIELDS_ARRAY [NSArray arrayWithObjects: @"会期",@"内容",@"出展者/講師",@"場所",@"詳細",nil]

@interface Exhibition : NSObject <NSXMLParserDelegate>{
    
    
}
@property (strong,nonatomic) NSMutableArray *Ex_name;
@property (strong,nonatomic) NSMutableArray *Ex_address;
@property (strong,nonatomic) NSMutableArray *Ex_dateS;
@property (strong,nonatomic) NSMutableArray *Ex_dateE;

@property (nonatomic) NSInteger selectedIndex;
@property (strong,nonatomic) NSMutableArray *parsedData,*dicData;
@property (strong,nonatomic) NSMutableArray *OSData;
@property (strong,nonatomic) NSMutableDictionary *dic;
@property (strong,nonatomic) NSDictionary *namedic,*keynum;
@property (nonatomic) NSString *nowTagStr,*nowTagStr2;
//@property (nonatomic) NSString *txtBuffer;
@property (nonatomic) NSInteger num;
@property (nonatomic) NSMutableString *CurrentStr;
@property (strong,nonatomic) data_ex *CurrentData;

//@property (weak,nonatomic,readonly,getter = getCurrentEx) NSMutableDictionary *current_Ex;
//@property (weak,nonatomic,readonly,getter = getIdForNewRecord) NSString *idForNewRecord;

//@property (strong,nonatomic) NSString *errorMessage;

//- (void) downloadData: (NSOperation *) doAtEndofCommunication;



- (void) setxmlparser;

- (void) parseDidStart: (NSXMLParser *)parser;

- (void) parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
     attributes:(NSDictionary *)attributeDict;

- (void) parser:(NSXMLParser *)parser
foundCharacters:(NSString *)string;

- (void) parser:(NSXMLParser *)parser
  didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName;

- (void) parserDidEndDocument:(NSXMLParser *)parser;
@end
