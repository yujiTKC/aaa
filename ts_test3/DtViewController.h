//
//  DtViewController.h
//  ts_test4
//
//  Created by 滝内 雄二 on 2013/06/28.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import <UIKit/UIKit.h>
#define ALL_FIELDS_ARRAY [NSArray arrayWithObjects: @"会期",@"内容",@"出展者/講師",@"場所",@"詳細",nil]
@class Exhibition;

@interface DtViewController : UITableViewController{

    Exhibition *Ex;
    NSArray *fields;
}
@property (weak,nonatomic) IBOutlet UITableViewCell *cell1;
@property (weak, nonatomic) IBOutlet UITableViewCell *cell2;
@property (weak, nonatomic) IBOutlet UITableViewCell *cell3;
@property (weak, nonatomic) IBOutlet UITableViewCell *cell4;
@property (weak, nonatomic) IBOutlet UITableViewCell *cell5;
@property (weak, nonatomic) IBOutlet UITextView *briefText;

@property NSString *thisKey;

@property NSInteger selectedCellNum;


@end
