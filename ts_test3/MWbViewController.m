//
//  MWbViewController.m
//  ts_test1_0
//
//  Created by 滝内 雄二 on 2013/07/18.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import "AdViewController.h"
#import "Exhibition.h"
#import "MWbViewController.h"

@interface MWbViewController ()

@end

@implementation MWbViewController
@synthesize mwv = _mwv;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *vcArray = self.navigationController.viewControllers;
    Ex = ((AdViewController *)[vcArray objectAtIndex:0]).exhibition;
    int nn = Ex.selectedIndex;
    NSString *strurl = [Ex.dicData[nn] objectForKey:@"mapurl"];
    
    [_mwv loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strurl]]];
	self.navigationItem.title = @"TOP";
    // Do any additional setup after loading the view.
}
/*
 - (void)dealloc {
 [_wv release];
 [super dealloc];
 }*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
