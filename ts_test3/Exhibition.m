//
//  Exhibition.m
//  ts_test3
//
//  Created by 滝内 雄二 on 2013/06/27.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import "Exhibition.h"

@implementation Exhibition
@synthesize Ex_name = _Ex_name;
@synthesize Ex_address = _Ex_address;
@synthesize selectedIndex = _selectedIndex;
@synthesize Ex_dateS = _Ex_dateS;
@synthesize Ex_dateE = _Ex_dateE;
@synthesize parsedData = _parsedData;
@synthesize OSData = _OSDAta;
@synthesize nowTagStr = _nowTagStr;
@synthesize nowTagStr2= _nowTagStr2;
@synthesize num = _num;
@synthesize dic = _dic;
@synthesize namedic = _namedic;
@synthesize keynum = _keynum;
@synthesize dicData = _dicData;
@synthesize CurrentStr = _CurrentStr;
@synthesize CurrentData = _CurrentData;

- (void) downloadData:(NSOperation *)doAtEndofCommunication{
//    NSLog(@"test");
}



- (void) setxmlparser{
    NSString *path;
    NSArray *feeds;
    
    _parsedData = [[NSMutableArray alloc] initWithCapacity:20];
    
    path = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    feeds = [NSArray arrayWithContentsOfFile:path];
    
    NSURL *myURL = [NSURL URLWithString:feeds[0]];
    
    NSXMLParser *myParser = [[NSXMLParser alloc] initWithContentsOfURL:myURL];
    NSLog(@"setXML");
    myParser.delegate = self;
    [myParser parse];
    
//    NSLog(@"parsedData = %@",parsedData);
    
}

- (void) parseDidStart:(NSXMLParser *)parser{
    //初期化
    NSLog(@"setXML_DidStart");
    _nowTagStr = @"";
    _nowTagStr2= @"";
//    _nowTagStr3= @"";
    
}

//START ELEMENT

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    /*
    NSLog(@"setXML_DidEl");
    if ([elementName isEqualToString:@"description"]){
        _nowTagStr = [NSString stringWithString:elementName];
        _txtBuffer = @"";
    }
    */
    if (qName) {
        elementName = qName;
    }
    
    if ([elementName isEqualToString:@"item"]) {
//        _dic = [[NSMutableDictionary alloc] initWithCapacity:3];
        _CurrentData = [[data_ex alloc] init];
        [_parsedData addObject:_CurrentData];
        return;
    }else if ([elementName isEqualToString:@"openSearch:totalResults"]) {
        _nowTagStr2 = [NSString stringWithString:elementName];
        return;
    }
    /*
    for (NSString *key in attributeDict){
        NSLog(@"key:%@ val:%@",key,[attributeDict objectForKey:key]);
        
    }*/
    
    if([elementName isEqualToString:@"description"]){
        _CurrentStr = [NSMutableString string];
    }else if([elementName isEqualToString:@"title"]) {
        _CurrentStr = [NSMutableString string];
    }else{
        _CurrentStr = nil;
    }
    
}

//FOUND CHAR

- (void)parser:(NSXMLParser *)parser
foundCharacters:(NSString *)string
{
/*
 0711
    if ([_nowTagStr isEqualToString:@"item"]){
//        NSLog(@"foundCharacters_item, %@, num:%u",string,_num);
        _num += 1;
        NSString *snum = [[NSString alloc]initWithFormat:@"%d",_num];
        [_dic setObject:string forKey:snum];
        
        
    }else if ([_nowTagStr2 isEqualToString:@"openSearch:totalResults"]){
        NSLog(@"openSearch is %@",string);
//        _num += 1;
    }
0711
 */
    if (_CurrentStr){
        [_CurrentStr appendString:string];
    }
    
}


//END ELEMENT

- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
{
    if (qName) {
        elementName = qName;
    }
    
    if ([elementName isEqualToString:@"description"]) {
//        [_dic setObject:_CurrentStr forKey:@"description"];
        _CurrentData.description = _CurrentStr;
    }else if ([elementName isEqualToString:@"title"]){
        _CurrentData.title = _CurrentStr;
//        NSLog(@"title is %@",_CurrentData.title);
//        [_dic setObject:_CurrentStr forKey:@"title"];
    }
    
//    if ([elementName isEqualToString:@"item"]){
//        NSLog(@"item is %@",_txtBuffer);
//        NSLog(@"dic 要素数: %d",[_dic count]);
/* 0711
        [_parsedData addObject:_dic];
//        NSLog(@"parsedData Size = %u",[_parsedData count]);
        
        
    }else if ([elementName isEqualToString:@"openSearch:totalResults"]){
        NSLog(@"didEnd_opensearch");
    }
     
//        NSLog(@"qName : %@ and nameS : %@",qName,namespaceURI);

0711 */
}

- (void) parserDidEndDocument:(NSXMLParser *)parser{
    
    NSLog(@"パース終了");
    
//    NSLog(@"count data : %d",[_parsedData count]);
    
    _dicData = [[NSMutableArray alloc] initWithCapacity:10];
    NSArray *valnames = [NSArray arrayWithObjects:@"展覧会名",@"内容",@"URL",@"期間",@"場所",@"mapURL" ,nil];
    NSArray *keys = [NSArray arrayWithObjects:@"name",@"brief",@"url",@"period",@"address",@"mapurl", nil];
    
    NSArray *nums = [NSArray arrayWithObjects:@"0",@"1",@"2",@"3",@"4",@"5", nil];
    
    _namedic = [[NSDictionary alloc] initWithObjects:valnames forKeys:keys];
    _keynum  = [[NSDictionary alloc] initWithObjects:keys forKeys:nums];
    
    for (data_ex *i in _parsedData){
        [i manageDescription];
//        _dic = [[NSMutableDictionary alloc] initWithCapacity:8];
        NSArray *vals = [NSArray arrayWithObjects:i.name,i.brief,i.url,i.period ,i.address,i.mapurl,nil];
        
        _dic = [[NSMutableDictionary alloc] initWithObjects:vals forKeys:keys];
        [_dicData addObject:_dic];
    }
}

@end
