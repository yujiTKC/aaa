//
//  AdViewController.m
//  ts_test3
//
//  Created by 滝内 雄二 on 2013/06/27.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import "AdViewController.h"
#import "Exhibition.h"
#import "DtViewController.h"
#import "data_ex.h"

@interface AdViewController ()

@end

@implementation AdViewController

@synthesize exhibition = _exhibition;

@synthesize num = _num;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /*
    NSBlockOperation *updateTable = [NSBlockOperation blockOperationWithBlock: ^(void) {
        [self.tableView reloadData];
    }];
    [self.exhibition downloadData : updateTable];
     */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    [self.exhibition setxmlparser];
    
    NSLog(@"calling: Adtableview:numberOfRowsInsection");
    return [self.exhibition.dicData count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"calling: Adtableview:cellForRowAtIndexPath");
//    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"ExhibitionCell"];
    //ストーリーボードでセルの設定をした場合、この1行で自動的にセルを再利用し必要なら生成する。
    //NSDictionary *currentRecord = self.exhibition//////////////////////////
    
    // Configure the cell...
//    NSLog(@"Ad index path = %d",indexPath.row);
    int nn = indexPath.row;
    cell.textLabel.text = [_exhibition.dicData[nn] objectForKey:@"name"];
//    NSMutableString *str = NSMutab
    
    
    NSLog(@"Ad index path %u , %@",indexPath.row,cell.textLabel.text);

    cell.detailTextLabel.text =[_exhibition.dicData[nn] objectForKey:@"period"];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    self.exhibition.selectedIndex = indexPath.row;
    NSLog(@"index path.row = %u",indexPath.row);
    _num = self.exhibition.selectedIndex;
    //何行目のセルをタップしたかを記憶

    //[self performSegueWithIdentifier:@"ex_segue" sender:self];
    
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ex_segue"]) {
        DtViewController *dest = (DtViewController*)[segue destinationViewController]; //移動先のビューコントローラ
        NSLog(@"SEGUE,_num = %u",_num);
//        NSLog(@"SEGUE sele %u",self.exhibition.selectedIndex);
        UITableViewCell *TappedCell = (UITableViewCell *)sender;
        dest.selectedCellNum = [self.tableView indexPathForCell:TappedCell].row;
//        NSLog(@"test %u",[self.tableView indexPathForCell:TappedCell].row);
    }
}
/*
if ([[segue identifier] isEqualToString:@"Identifierにつけた名前"]) {
    PlayViewController *viewController = (PlayViewController*)[segue destinationViewController];
    viewController.mode =@"受け渡す値";
}*/


@end
