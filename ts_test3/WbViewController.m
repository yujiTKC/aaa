//
//  WbViewController.m
//  ts_test1_0
//
//  Created by 滝内 雄二 on 2013/07/18.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import "AdViewController.h"
#import "Exhibition.h"
#import "WbViewController.h"

@interface WbViewController ()

@end

@implementation WbViewController
@synthesize wv = _wv;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *vcArray = self.navigationController.viewControllers;
    Ex = ((AdViewController *)[vcArray objectAtIndex:0]).exhibition;
    int nn = Ex.selectedIndex;
    NSString *strurl = [Ex.dicData[nn] objectForKey:@"url"];
    
    [_wv loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strurl]]];
	// Do any additional setup after loading the view.
}
/*
- (void)dealloc {
    [_wv release];
    [super dealloc];
}*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
