//
//  CheckBoxButton.h
//  ts_test1_0
//
//  Created by 滝内 雄二 on 2013/07/23.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckBoxButton : UIButton

@property (nonatomic,assign) BOOL checkBoxSelected;

- (void) setState: (CheckBoxButton*) button
             val : (BOOL) b;

@end


