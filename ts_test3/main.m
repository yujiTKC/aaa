//
//  main.m
//  ts_test3
//
//  Created by 滝内 雄二 on 2013/06/27.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
