//
//  AdViewController.h
//  ts_test3
//
//  Created by 滝内 雄二 on 2013/06/27.
//  Copyright (c) 2013年 滝内 雄二. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Exhibition;

@interface AdViewController : UITableViewController

@property (strong,nonatomic) IBOutlet Exhibition *exhibition;

@property (nonatomic) NSInteger num;
@end
